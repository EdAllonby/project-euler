﻿// The prime 41, can be written as the sum of six consecutive primes:
// 41 = 2 + 3 + 5 + 7 + 11 + 13
// This is the longest sum of consecutive primes that adds to a prime below one-hundred.
// The longest sum of consecutive primes below one-thousand that adds to a prime, contains 21 terms, and is equal to 953.
// Which prime, below one-million, can be written as the sum of the most consecutive primes?

using System;
using System.Collections.Generic;
using System.Linq;
using SharedUtilities;

namespace Solutions.Problems
{
    internal class Problem50 : IProjectEulerSolution
    {
        public string Solve()
        {
            IEnumerable<int> primes = MathsUtilities.GetAllPrimeNumbers(1000000);

            return primes.Count().ToString();
        }
    }
}