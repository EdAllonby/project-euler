﻿// If the numbers 1 to 5 are written out in words: one, two, three, four, five, then there are 3 + 3 + 5 + 4 + 4 = 19 letters used in total.
// If all the numbers from 1 to 1000 (one thousand) inclusive were written out in words, how many letters would be used?
// NOTE: Do not count spaces or hyphens. For example, 342 (three hundred and forty-two) contains 23 letters and 115 (one hundred and fifteen) contains 20 letters.
// The use of "and" when writing out numbers is in compliance with British usage.

using System;
using System.Collections.Generic;
using System.Linq;

namespace Solutions.Problems
{
    internal sealed class Problem17 : IProjectEulerSolution
    {
        private Dictionary<int, string> numberDefinitions;
        
        public string Solve()
        {
            numberDefinitions = new Dictionary<int, string>
            {
                {0, ""},
                {1, "one"},
                {2, "two"},
                {3, "three"},
                {4, "four"},
                {5, "five"},
                {6, "six"},
                {7, "seven"},
                {8, "eight"},
                {9, "nine"},
                {10, "ten"},
                {11, "eleven"},
                {12, "twelve"},
                {13, "thirteen"},
                {14, "fourteen"},
                {15, "fifteen"},
                {16, "sixteen"},
                {17, "seventeen"},
                {18, "eighteen"},
                {19, "nineteen"},
                {20, "twenty"},
                {30, "thirty"},
                {40, "forty"},
                {50, "fifty"},
                {60, "sixty"},
                {70, "seventy"},
                {80, "eighty"},
                {90, "ninety"},
                {100, "hundred"},
                {1000, "thousand"}
            };

            var numbers = new List<string>();
            for (int i = 0; i < 1000; i++)
            {
                numbers.Add(NumberParser(i));
            }
            Console.ReadKey();

            return 0.ToString();
        }

        private string NumberParser(int number)
        {
            List<int> individualDigits = GetIntList(number);
            string englishNumber = IntegerToEnglish(individualDigits);
            return englishNumber;
        }

        private string IntegerToEnglish(IReadOnlyList<int> getIndividualNumbers)
        {
            int counter = 0;

            string numberInEnglish = "";

            if (SumIntegerList(getIndividualNumbers) <= 10 || SumIntegerList(getIndividualNumbers) >= 20 && SumIntegerList(getIndividualNumbers) < 100)
            {
                foreach (int number in getIndividualNumbers.Reverse())
                {
                    int test = number*(int) Math.Pow(10, counter);
                    numberInEnglish = numberDefinitions[test] + numberInEnglish;
                    counter++;
                }
            }

            // Stupid teen number checks
            if (SumIntegerList(getIndividualNumbers) > 10 && SumIntegerList(getIndividualNumbers) < 20)
            {
                return numberDefinitions[10 + getIndividualNumbers[1]];
            }

            //if number is greater than 100
            if (SumIntegerList(getIndividualNumbers) > 100)
            {
                numberInEnglish = numberDefinitions[getIndividualNumbers[2]] + numberDefinitions[100];

                foreach (int number in getIndividualNumbers.Reverse())
                {
                    int test = number*(int) Math.Pow(10, counter);
                    numberInEnglish = numberDefinitions[test] + numberInEnglish;
                    counter++;
                    if (counter == 3)
                    {
                        break;
                    }
                }
            }

            return numberInEnglish;


//            if (SumIntegerList(getIndividualNumbers) <= 20)
//            {
//                if (getIndividualNumbers.Count == 1)
//                {
//                    return numberDefinitions[getIndividualNumbers[0]];
//                }
//                return numberDefinitions[10 + getIndividualNumbers[1]];
//            }
//            if (SumIntegerList(getIndividualNumbers) < 100)
//            {
//                string firstVal = numberDefinitions[getIndividualNumbers[0] *10 ];
//                string secondVal = numberDefinitions[getIndividualNumbers[1]];
//                return firstVal + secondVal;
//            }
//            return null;
        }

        private int SumIntegerList(IReadOnlyCollection<int> intList)
        {
            int digitPosition = 0;
            int numberOfDigits = intList.Count;
            int number = 0;

            foreach (int integer in intList)
            {
                int currentPosition = numberOfDigits - digitPosition;

                int DecimalPosition = IntegerPositionToDecimal(currentPosition);

                number += integer*DecimalPosition;

                digitPosition++;
            }
            return number;
        }

        private static int IntegerPositionToDecimal(int currentPosition)
        {
            string zeros = null;

            for (int i = 0; i < currentPosition; i++)
            {
                if (i != 0)
                {
                    zeros += "0";
                }
            }
            return Int32.Parse("1" + zeros);
        }

        private static List<int> GetIntList(int number)
        {
            var listOfIntegers = new List<int>();
            if (number == 0)
            {
                return new List<int> {0};
            }
            while (number > 0)
            {
                listOfIntegers.Add(number%10);
                number = number/10;
            }
            listOfIntegers.Reverse();
            return listOfIntegers.ToList();
        }
    }
}