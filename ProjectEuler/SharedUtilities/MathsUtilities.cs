﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;

namespace SharedUtilities
{
    public static class MathsUtilities
    {
        /// <summary>
        /// An optimised implementation for finding primes.
        /// </summary>
        /// <param name="upperBound">The maximum prime to find.</param>
        /// <returns>A collection of primes up to the <see cref="upperBound"/> defined.</returns>
        public static IEnumerable<int> GetAllPrimeNumbers(int upperBound)
        {
            return SieveOfEratosthenes(upperBound);
        }

        public static int GetNthPrime(int nthPrime)
        {
            List<int> largeCollectionOfPrimes = SieveOfEratosthenes(1000000).ToList();
            return largeCollectionOfPrimes[nthPrime - 1];
        }

        public static bool IsAPrimeNumber(int number)
        {
            int boundary = (int)Math.Floor(Math.Sqrt(number));

            if (number == 1) return false;
            if (number == 2) return true;

            for (int i = 2; i <= boundary; ++i)
            {
                if (number % i == 0) return false;
            }

            return true;
        }

        /// <summary>
        /// Creates the number of fibonacci numbers up to the specified count. Uses recursive implementation.
        /// </summary>
        /// <param name="count">How many fibonacci numbers to find.</param>
        /// <returns>The fibonacci numbers found.</returns>
        public static List<int> FibonacciGeneratorFromCount(int count)
        {
            var numbers = new List<int>();

            for (int i = 0; i <= count; i++)
            {
                numbers.Add(FibonacciFinder(i));
            }

            return numbers;
        }

        /// <summary>
        /// Creates fibonacci numbers up to the specified upper bound. Uses recursive implementation.
        /// </summary>
        /// <param name="maxFibonacciNumber">Specifies the last number to look for in the fibonacci sequence. If Not a fibonacci number, then use the last one found.</param>
        /// <returns>The fubonacci numbers found.</returns>
        public static IEnumerable<int> FibonacciGeneratorFromMaxNumber(int maxFibonacciNumber)
        {
            var numbers = new List<int>();

            for (int i = 0; ; i++)
            {
                int currentFibonacciNumber = FibonacciFinder(i);

                if (currentFibonacciNumber > maxFibonacciNumber)
                {
                    break;
                }

                numbers.Add(currentFibonacciNumber);
            }

            return numbers;
        }

        public static List<int> FactorFinder(int number)
        {
            List<int> factors = new List<int>();
            int max = (int) Math.Sqrt(number);
            for (int factor = 1; factor <= max; ++factor)
            {
                if (number%factor == 0)
                {
                    factors.Add(factor);
                    if (factor != number/factor)
                    {
                        factors.Add(number/factor);
                    }
                }
            }
            return factors;
        }

        public static bool IsPalindrome(int possiblePalindrome)
        {
            string palindrome = possiblePalindrome.ToString();

            char[] tempPalindrome = palindrome.ToCharArray();
            Array.Reverse(tempPalindrome);
            string reversePalindrome = new string(tempPalindrome);

            return palindrome.Equals(reversePalindrome);
        }

        private static IEnumerable<int> SieveOfEratosthenes(int upperBound)
        {
            const double LegendresConstant = 1.08366;

            var values = new List<int>((int)(upperBound / (Math.Log(upperBound) - LegendresConstant)));

            double upperBoundSquareRoot = Math.Sqrt(upperBound);

            var eliminated = new BitArray(upperBound + 1);

            values.Add(2);

            for (int possiblePrime = 3; possiblePrime <= upperBound; possiblePrime += 2)
            {
                if (!eliminated[possiblePrime])
                {
                    if (possiblePrime < upperBoundSquareRoot)
                    {
                        for (int eliminatedPrimeLookup = possiblePrime * possiblePrime; eliminatedPrimeLookup <= upperBound; eliminatedPrimeLookup += 2 * possiblePrime)
                        {
                            eliminated[eliminatedPrimeLookup] = true;
                        }
                    }

                    values.Add(possiblePrime);
                }
            }

            return values;
        }

        private static int FibonacciFinder(int upperBound)
        {
            if (upperBound <= 2)
            {
                return upperBound == 0 ? 0 : 1;
            }

            return FibonacciFinder(upperBound - 1) + FibonacciFinder(upperBound - 2);
        }

        public static int GetSumOfDigits(BigInteger number)
        {
            return number.ToString().Sum(digit => Int32.Parse(digit.ToString()));
        }
    }
}