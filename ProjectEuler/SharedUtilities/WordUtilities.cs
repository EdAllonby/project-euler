﻿using System.Collections.Generic;
using System.Linq;

namespace SharedUtilities
{
    public static class WordUtilities
    {
        /// <summary>
        /// Find all permutations of a string
        /// </summary>
        /// <param name="word">string to find permutations for.</param>
        /// <returns>A list of permutations for the input string.</returns>
        public static IEnumerable<string> FindPermutations(string word)
        {
            if (word.Length == 1)
            {
                return new List<string> {word};
            }

            if (word.Length == 2)
            {
                char[] character = word.ToCharArray();
                string s = new string(new[] {character[1], character[0]});
                return new List<string> {word, s};
            }

            List<string> result = new List<string>();

            IEnumerable<string> subsetPermutations = FindPermutations(word.Substring(1));
            char firstChar = word[0];

            foreach (string temp in subsetPermutations.Select(s => firstChar + s))
            {
                result.Add(temp);
                char[] chars = temp.ToCharArray();
                for (int i = 0; i < temp.Length - 1; i++)
                {
                    char t = chars[i];
                    chars[i] = chars[i + 1];
                    chars[i + 1] = t;
                    string s2 = new string(chars);
                    result.Add(s2);
                }
            }

            return result.ToList();
        }
    }
}